package project.mkungonda.corral;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import project.mkungonda.corral.data.DatabaseDescription.List;
import project.mkungonda.corral.data.DatabaseDescription.ListItems;

public class AddEditFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public interface AddEditFragmentListener {
        void onAddEditCompleted(Uri listUri);
    }

    private static final int LIST_LOADER = 0;

    private TextInputLayout nameTextInputLayout;
    private EditText requiredItem;
    private AddEditFragmentListener listener;
    private FloatingActionButton savListFAB;
    private CoordinatorLayout coordinatorLayout;
    private Button addItemButton;
    private Spinner typeSpinner;
    private boolean addingNewList = true;
    private Uri listUri;
    private java.util.List<EditText> items;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (AddEditFragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        items = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_add_edit, container, false);

        nameTextInputLayout = (TextInputLayout) view.findViewById(R.id.nameTextInputLayout);
        requiredItem = (EditText) view.findViewById(R.id.item);
        requiredItem.addTextChangedListener(nameChangedListener);
        nameTextInputLayout.getEditText().addTextChangedListener(nameChangedListener);

        addItemButton = (Button) view.findViewById(R.id.addItemButton);
        addItemButton.setOnClickListener(addItemButtonClicked);

        typeSpinner = (Spinner) view.findViewById(R.id.typeTextInputLayout);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, List.TYPE_OPTIONS);
        typeSpinner.setAdapter(adapter);

        savListFAB = (FloatingActionButton) view.findViewById(R.id.saveFloatingActionButton);
        savListFAB.setOnClickListener(saveListButtonClicked);
        updateSaveButtonFAB();

        coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(R.id.coordinatorLayout);

        Bundle arguments = getArguments();

        if (arguments != null) {
            addingNewList = false;
            listUri = arguments.getParcelable(MainActivity.LIST_URI);
        }

        if (listUri != null)
            getLoaderManager().initLoader(LIST_LOADER, null, this);

        return view;
    }

    private final TextWatcher nameChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateSaveButtonFAB();
        }

        @Override
        public void afterTextChanged(Editable s) { }
    };

    private void updateSaveButtonFAB() {
        String name = nameTextInputLayout.getEditText().getText().toString();
        String item = requiredItem.getText().toString();

        if ((name.trim().length() != 0) && (item.trim().length() != 0)) {
            savListFAB.show();
        } else {
            savListFAB.hide();
        }

    }

    private final View.OnClickListener saveListButtonClicked = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    saveList();
                }
            };

    private final View.OnClickListener addItemButtonClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout itemsContainer = (LinearLayout) getView().findViewById(R.id.itemsContainer);
            EditText item = new EditText(getContext());
            item.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            item.setId(R.id.item_id);
            item.setHint(R.string.hint_item_name);
            item.setInputType(InputType.TYPE_CLASS_TEXT);
            item.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            itemsContainer.addView(item);
            items.add(item);
        }
    };

    private void saveList() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        items.add((EditText) getView().findViewById(R.id.item));
        ContentValues contentValues = new ContentValues();

        contentValues.put(List.COLUMN_NAME, nameTextInputLayout.getEditText().getText().toString());
        contentValues.put(List.COLUMN_QUANTITY, items.size());
        contentValues.put(List.COLUMN_TYPE, typeSpinner.getSelectedItem().toString());
        contentValues.put(List.COLUMN_UPDATED_AT, dateFormat.format(date));

        if (addingNewList) {
            Uri newListUri = getActivity().getContentResolver().insert(List.CONTENT_URI, contentValues);
            if (newListUri != null) {
                int listId = Integer.parseInt(newListUri.getLastPathSegment());

                for(int i=0; i < items.size(); i++) {
                    contentValues.clear();
                    contentValues.put(ListItems.COLUMN_LIST_ID, listId);
                    contentValues.put(ListItems.COLUMN_NAME, items.get(i).getText().toString());
                    Uri newItemUri = getActivity().getContentResolver().insert(ListItems.CONTENT_URI, contentValues);
                    System.out.println(newItemUri);
                    contentValues.clear();
                }

                Snackbar.make(coordinatorLayout, R.string.list_added, Snackbar.LENGTH_LONG).show();
                listener.onAddEditCompleted(newListUri);
            } else {
                Snackbar.make(coordinatorLayout, R.string.list_not_added, Snackbar.LENGTH_LONG).show();
            }
        } else {
            int updatedRows = getActivity().getContentResolver().update(listUri, contentValues, null, null);

            if (updatedRows > 0) {
                listener.onAddEditCompleted(listUri);
                Snackbar.make(coordinatorLayout, R.string.list_updated, Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(coordinatorLayout, R.string.list_not_updated, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LIST_LOADER:
                return new CursorLoader(getActivity(), listUri, null, null, null, null);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if (data != null && data.moveToFirst()) {

            int nameIndex       = data.getColumnIndex(List.COLUMN_NAME);
            int typeIndex       = data.getColumnIndex(List.COLUMN_TYPE);

            nameTextInputLayout.getEditText().setText(data.getString(nameIndex));
            typeSpinner.setSelection(Arrays.asList(List.TYPE_OPTIONS).indexOf(data.getString(typeIndex)));
            updateSaveButtonFAB();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }
}


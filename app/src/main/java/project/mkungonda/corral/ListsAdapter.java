package project.mkungonda.corral;

import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.database.Cursor;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.support.v7.widget.RecyclerView;

import project.mkungonda.corral.data.DatabaseDescription.List;

public class ListsAdapter extends RecyclerView.Adapter<ListsAdapter.ViewHolder> {

    public interface ListClickListener {
        void onClick(Uri listUri);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView textName;
        public final TextView textType;
        public final TextView textQuantity;
        public final TextView textTime;

        private long rowID;

        public ViewHolder(View itemView) {
            super(itemView);
            textName            = (TextView) itemView.findViewById(R.id.textName);
            textType            = (TextView) itemView.findViewById(R.id.textType);
            textQuantity        = (TextView) itemView.findViewById(R.id.textQuantity);
            textTime            = (TextView) itemView.findViewById(R.id.textTime);

            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            clickListener.onClick(List.buildListUri(rowID));
                        }
                    }
            );
        }

        public void setRowID(long rowID) {
            this.rowID = rowID;
        }
    }

    private Cursor cursor = null;
    private final ListClickListener clickListener;

    public ListsAdapter(ListClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        cursor.moveToPosition(position);
        holder.setRowID(cursor.getLong(cursor.getColumnIndex(List._ID)));
        holder.textName.setText(cursor.getString(cursor.getColumnIndex(List.COLUMN_NAME)));
        holder.textType.setText(cursor.getString(cursor.getColumnIndex(List.COLUMN_TYPE)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if(cursor.getInt(cursor.getColumnIndex(List.COLUMN_STATUS)) == List.STATUS_CLOSED) {
                holder.textQuantity.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_done_green_24dp, 0, 0,0);
                holder.textQuantity.setText(null);
            } else {
                holder.textQuantity.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                holder.textQuantity.setText(cursor.getString(cursor.getColumnIndex(List.COLUMN_QUANTITY)));
            }
        }
        holder.textTime.setText(cursor.getString(cursor.getColumnIndex(List.COLUMN_UPDATED_AT)));
    }

    @Override
    public int getItemCount() {
        return (cursor != null) ? cursor.getCount() : 0;
    }

    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }
}

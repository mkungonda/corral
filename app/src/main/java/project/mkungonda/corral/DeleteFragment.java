package project.mkungonda.corral;

import android.net.Uri;
import android.os.Bundle;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v4.app.DialogFragment;

public class DeleteFragment extends DialogFragment {

    private DetailFragment.DetailFragmentListener listener;
    private Uri listUri;

    public DeleteFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (DetailFragment.DetailFragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
        listUri  = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle bundle) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.confirm_title);
        builder.setMessage(R.string.confirm_message);

        builder.setPositiveButton(R.string.button_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int button) {
                        Bundle arguments = getArguments();
                        if (arguments != null) {
                            listUri = arguments.getParcelable(MainActivity.LIST_URI);
                            getActivity().getContentResolver().delete(listUri, null, null);
                            listener.onListDeleted();
                        }
                    }
                }
        );
        builder.setNegativeButton(R.string.button_cancel, null);
        return builder.create();
    }
}
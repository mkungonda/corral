package project.mkungonda.corral;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import project.mkungonda.corral.data.DatabaseDescription;
import project.mkungonda.corral.data.DatabaseDescription.List;
import project.mkungonda.corral.data.DatabaseDescription.ListItems;

public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public interface DetailFragmentListener {
        void onListDeleted();
        void onEditList(Uri listUri);
        void onStatusUpdate(Uri listUri);
    }

    private static final int LIST_LOADER = 0;

    private DetailFragmentListener listener;
    private Uri listUri;
    private MenuItem closeActionItem;
    private int currentStatus;
    private TextView nameTextView;
    private TextView typeTextView;
    private TextView statusTextView;
    private CoordinatorLayout coordinatorLayout;
    private LinearLayout itemsContainer;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (DetailFragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        coordinatorLayout   = (CoordinatorLayout) getActivity().findViewById(R.id.coordinatorLayout);
        Bundle arguments    = getArguments();
        setHasOptionsMenu(true);

        if (arguments != null)
            listUri = arguments.getParcelable(MainActivity.LIST_URI);

        View view = inflater.inflate(R.layout.fragment_details, container, false);
        itemsContainer  = (LinearLayout) view.findViewById(R.id.itemsDetailsContainer);
        nameTextView    = (TextView) view.findViewById(R.id.nameTextView);
        typeTextView    = (TextView) view.findViewById(R.id.typeTextView);
        statusTextView  = (TextView) view.findViewById(R.id.statusTextView);

        getLoaderManager().initLoader(LIST_LOADER, null, this);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.fragment_details_menu, menu);
        closeActionItem = menu.findItem(R.id.action_close);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                listener.onEditList(listUri);
                return true;
            case R.id.action_close:
                toggleListStatus();
                return true;
            case R.id.action_delete:
                deleteList();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void toggleListStatus() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseDescription.List.COLUMN_STATUS, currentStatus);
        contentValues.put(DatabaseDescription.List.COLUMN_UPDATED_AT, dateFormat.format(date));

        int updatedRows = getActivity().getContentResolver().update(listUri, contentValues, null, null);

        if (updatedRows > 0) {
            switch (currentStatus) {
                case List.STATUS_OPEN:
                    Snackbar.make(coordinatorLayout, R.string.list_opened, Snackbar.LENGTH_LONG).show();
                    break;
                case List.STATUS_CLOSED:
                    Snackbar.make(coordinatorLayout, R.string.list_closed, Snackbar.LENGTH_LONG).show();
                    break;
            }
            listener.onStatusUpdate(listUri);
        } else {
            Snackbar.make(coordinatorLayout, R.string.list_not_updated, Snackbar.LENGTH_LONG).show();
        }
    }

    private void deleteList() {
        Bundle arguments = new Bundle();
        arguments.putParcelable(MainActivity.LIST_URI, listUri);
        confirmDelete.setArguments(arguments);
        confirmDelete.show(getFragmentManager(), "confirm delete");
    }

    private final DialogFragment confirmDelete =  new DeleteFragment();

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;

        switch (id) {
            case LIST_LOADER:
                cursorLoader = new CursorLoader(getActivity(), listUri, null, null, null, null);
                break;
            default:
                cursorLoader = null;
                break;
        }

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data != null && data.moveToFirst()) {
            int nameIndex       = data.getColumnIndex(List.COLUMN_NAME);
            int typeIndex       = data.getColumnIndex(List.COLUMN_TYPE);
            int statusIndex     = data.getColumnIndex(List.COLUMN_STATUS);

            Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(data));

            if (closeActionItem != null) {
                closeActionItem.setEnabled(true);
                if(data.getInt(statusIndex) == List.STATUS_CLOSED) {
                    currentStatus = List.STATUS_OPEN;
                    statusTextView.setText(R.string.label_status_closed);
                    statusTextView.setTextColor(Color.RED);
                    closeActionItem.setIcon(R.drawable.ic_assignment_white_24dp);
                } else {
                    currentStatus = List.STATUS_CLOSED;
                    statusTextView.setText(R.string.label_status_open);
                    statusTextView.setTextColor(Color.GREEN);
                    closeActionItem.setIcon(R.drawable.ic_assignment_turned_in_white_24dp);
                }
            }

            nameTextView.setText(data.getString(nameIndex));
            typeTextView.setText(data.getString(typeIndex));

            int counter = 1;
            do {
                int id          = data.getColumnIndex(ListItems._ID);
                int name        = data.getColumnIndex(ListItems.COLUMN_NAME);
                TextView item   = new TextView(getContext());

                item.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                item.setId(data.getInt(id));
                item.setText(counter + " " + data.getString(name));
                item.setBackgroundResource(R.drawable.item_border);
                item.setPadding(5, 5, 5, 5);
                item.setTextSize(16);
                item.setTextColor(Color.BLACK);
                itemsContainer.addView(item);
                counter++;

            } while (data.moveToNext());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }
}


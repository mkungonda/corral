package project.mkungonda.corral;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements ListsFragment.ListFragmentListener,
        DetailFragment.DetailFragmentListener, AddEditFragment.AddEditFragmentListener, StatsFragment.StatsFragmentListener {

    public static final String LIST_URI = "list_uri";

    private ListsFragment listsFragment;

    static {
        System.loadLibrary("stats-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null && findViewById(R.id.fragmentContainer) != null) {
            listsFragment = new ListsFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragmentContainer, listsFragment);
            transaction.commit();
        } else {
            listsFragment = (ListsFragment) getSupportFragmentManager().findFragmentById(R.id.listFragment);
        }
    }

    @Override
    public void onListSelected(Uri listUri) {
        if (findViewById(R.id.fragmentContainer) != null)
            displayList(listUri, R.id.fragmentContainer);
        else {
            getSupportFragmentManager().popBackStack();
            displayList(listUri, R.id.rightPaneContainer);
        }
    }

    @Override
    public void onAddList() {
        if (findViewById(R.id.fragmentContainer) != null)
            displayAddEditFragment(R.id.fragmentContainer, null);
        else
            displayAddEditFragment(R.id.rightPaneContainer, null);
    }

    @Override
    public void onStatsShow() {
        displayStatsFragment(R.id.fragmentContainer, null);
    }

    private void displayList(Uri listUri, int viewID) {
        DetailFragment detailFragment = new DetailFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(LIST_URI, listUri);
        detailFragment.setArguments(arguments);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, detailFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void displayAddEditFragment(int viewID, Uri listUri) {
        AddEditFragment addEditFragment = new AddEditFragment();

        if (listUri != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(LIST_URI, listUri);
            addEditFragment.setArguments(arguments);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, addEditFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void displayStatsFragment(int viewID, Uri listUri) {
        StatsFragment statsFragment = new StatsFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(viewID, statsFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onListDeleted() {
        getSupportFragmentManager().popBackStack();
        listsFragment.updateList();
    }

    @Override
    public void onEditList(Uri listUri) {
        if (findViewById(R.id.fragmentContainer) != null) {
            displayAddEditFragment(R.id.fragmentContainer, listUri);
        } else {
            displayAddEditFragment(R.id.rightPaneContainer, listUri);
        }
    }

    @Override
    public void onAddEditCompleted(Uri listUri) {
        getSupportFragmentManager().popBackStack();
        if(listsFragment != null)
            listsFragment.updateList();

        if (findViewById(R.id.fragmentContainer) == null) {
            getSupportFragmentManager().popBackStack();
            displayList(listUri, R.id.rightPaneContainer);
        }
    }

    @Override
    public void onStatusUpdate(Uri listUri) {
        onAddEditCompleted(listUri);
    }
}

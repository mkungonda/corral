package project.mkungonda.corral.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseDescription {
    public static final String AUTHORITY = "project.mkungonda.corral.data";

    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final class List implements BaseColumns {
        public static final String TABLE_NAME       = "list";
        public static final String TABLE_NAME_ITEMS = "list_items";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        public static final Uri STATS_URI   = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME_ITEMS).build();

        public static final String COLUMN_NAME          = "list_name";
        public static final String COLUMN_TYPE          = "list_type";
        public static final String COLUMN_QUANTITY      = "list_quantity";
        public static final String COLUMN_STATUS        = "list_status";
        public static final String COLUMN_CREATED_AT    = "list_created_at";
        public static final String COLUMN_UPDATED_AT    = "list_updated_at";
        public static final String[] TYPE_OPTIONS       = new String[] { "Shopping list", "Todo", "Other" };
        public static final int STATUS_OPEN             = 1;
        public static final int STATUS_CLOSED           = 2;

        public static Uri buildListUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ListItems implements BaseColumns {
        public static final String TABLE_NAME = "list_items";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        public static final String COLUMN_NAME          = "list_items_name";
        public static final String COLUMN_LIST_ID       = "list_items_list_id";
        public static final String COLUMN_TYPE          = "list_items_type";
        public static final String COLUMN_QUANTITY      = "list_items_quantity";
        public static final String COLUMN_STATUS        = "list_items_status";
        public static final String COLUMN_CREATED_AT    = "list_items_created_at";
        public static final String COLUMN_UPDATED_AT    = "list_items_updated_at";

        public static final int STATUS_PENDING          = 1;
        public static final int STATUS_COMPLETED        = 2;

        public static Uri buildListUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}

package project.mkungonda.corral.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import project.mkungonda.corral.R;
import project.mkungonda.corral.data.DatabaseDescription.List;
import project.mkungonda.corral.data.DatabaseDescription.ListItems;

public class CorralContentProvider extends ContentProvider {
    private CorralDatabaseHelper dbHelper;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int ONE_LIST       = 1;
    private static final int LISTS          = 2;
    private static final int ONE_LIST_ITEM  = 4;
    private static final int LIST_ITEMS     = 8;

    static {
        uriMatcher.addURI(DatabaseDescription.AUTHORITY, List.TABLE_NAME + "/#", ONE_LIST);
        uriMatcher.addURI(DatabaseDescription.AUTHORITY, List.TABLE_NAME, LISTS);
        uriMatcher.addURI(DatabaseDescription.AUTHORITY, ListItems.TABLE_NAME + "/#", ONE_LIST_ITEM);
        uriMatcher.addURI(DatabaseDescription.AUTHORITY, ListItems.TABLE_NAME, LIST_ITEMS);
    }

    public CorralContentProvider() {
    }

    @Override
    public boolean onCreate() {
        dbHelper = new CorralDatabaseHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)) {
            case ONE_LIST:
                queryBuilder.setTables(List.TABLE_NAME + " list INNER JOIN " + ListItems.TABLE_NAME + " item ON " + "list." + List._ID + " = item." + ListItems.COLUMN_LIST_ID);
                queryBuilder.appendWhere("list." + List._ID + "=" + uri.getLastPathSegment());
                break;
            case ONE_LIST_ITEM:
                queryBuilder.setTables(List.TABLE_NAME + " list INNER JOIN " + ListItems.TABLE_NAME + " item ON " + "list." + List._ID + " = item." + ListItems.COLUMN_LIST_ID);
                queryBuilder.appendWhere("item." + ListItems._ID + "=" + uri.getLastPathSegment());
                break;
            case LISTS:
                queryBuilder.setTables(List.TABLE_NAME);
                break;
            case LIST_ITEMS:
                queryBuilder.setTables(List.TABLE_NAME + " list INNER JOIN " + ListItems.TABLE_NAME + " item ON " + "list." + List._ID + " = item." + ListItems.COLUMN_LIST_ID);
                break;
            default:
            throw new UnsupportedOperationException( getContext().getString(R.string.invalid_query_uri) + uri);
        }

        Cursor cursor = queryBuilder.query(dbHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri newListUri;

        switch (uriMatcher.match(uri)) {
            case LISTS:
                long listRowId = dbHelper.getWritableDatabase().insert(List.TABLE_NAME, null, values);

                if(listRowId > 0) {
                    newListUri = List.buildListUri(listRowId);
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                else
                    throw new SQLException(getContext().getString(R.string.invalid_insert_uri));
                break;
            case LIST_ITEMS:
                long itemRowId = dbHelper.getWritableDatabase().insert(ListItems.TABLE_NAME, null, values);

                if(itemRowId > 0) {
                    newListUri = ListItems.buildListUri(itemRowId);
                }
                else
                    throw new SQLException(getContext().getString(R.string.invalid_insert_uri));
                break;
            default:
                throw new UnsupportedOperationException(getContext().getString(R.string.invalid_insert_uri) + uri);
        }

        return newListUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int numberOfRowsUpdated;

        switch (uriMatcher.match(uri)) {
            case ONE_LIST:
                String id = uri.getLastPathSegment();

                numberOfRowsUpdated = dbHelper.getWritableDatabase().update(List.TABLE_NAME, values, List._ID + "=" + id, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException(getContext().getString(R.string.invalid_update_uri));
        }

        if (numberOfRowsUpdated !=0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return numberOfRowsUpdated;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int numberOfRowsDeleted;

        switch (uriMatcher.match(uri)) {
            case ONE_LIST:
                String listId = uri.getLastPathSegment();
                numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(List.TABLE_NAME, List._ID + "=" + listId, selectionArgs);
                break;
            case ONE_LIST_ITEM:
                String listItemId = uri.getLastPathSegment();
                numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(ListItems.TABLE_NAME, List._ID + "=" + listItemId, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException(getContext().getString(R.string.invalid_delete_uri) + uri);
        }

        if(numberOfRowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return numberOfRowsDeleted;
    }
}

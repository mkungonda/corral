package project.mkungonda.corral.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import project.mkungonda.corral.data.DatabaseDescription.List;
import project.mkungonda.corral.data.DatabaseDescription.ListItems;

public class CorralDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Corral.db";
    private static final int DATABASE_VERSION = 8;


    public CorralDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String Create_LISTS_TABLE = "CREATE TABLE " + List.TABLE_NAME + " (" +
                List._ID + " integer primary key, " +
                List.COLUMN_NAME + " TEXT, " +
                List.COLUMN_TYPE + " TEXT, " +
                List.COLUMN_QUANTITY + " INTEGER DEFAULT 0, " +
                List.COLUMN_STATUS + " INTEGER DEFAULT " + List.STATUS_OPEN + "," +
                List.COLUMN_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                List.COLUMN_UPDATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP);";

        db.execSQL(Create_LISTS_TABLE);

        final String Create_ITEMS_TABLE = "CREATE TABLE " + ListItems.TABLE_NAME + " (" +
                ListItems._ID + " integer primary key, " +
                ListItems.COLUMN_LIST_ID + " integer," +
                ListItems.COLUMN_NAME + " TEXT, " +
                ListItems.COLUMN_TYPE + " TEXT, " +
                ListItems.COLUMN_QUANTITY + " INTEGER DEFAULT 1, " +
                ListItems.COLUMN_STATUS + " INTEGER DEFAULT " + ListItems.STATUS_PENDING + "," +
                ListItems.COLUMN_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                ListItems.COLUMN_UPDATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                " FOREIGN KEY (" + ListItems.COLUMN_LIST_ID + ") REFERENCES " + List.TABLE_NAME + "(" + List._ID + "));";

        db.execSQL(Create_ITEMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String Drop_List = "DROP TABLE " + List.TABLE_NAME;
        db.execSQL(Drop_List);
        Drop_List = "DROP TABLE " + ListItems.TABLE_NAME;
        db.execSQL(Drop_List);

        final String Create_LISTS_TABLE = "CREATE TABLE " + List.TABLE_NAME + " (" +
                List._ID + " integer primary key, " +
                List.COLUMN_NAME + " TEXT, " +
                List.COLUMN_TYPE + " TEXT, " +
                List.COLUMN_QUANTITY + " INTEGER DEFAULT 0, " +
                List.COLUMN_STATUS + " INTEGER DEFAULT " + List.STATUS_OPEN + "," +
                List.COLUMN_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                List.COLUMN_UPDATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP);";

        db.execSQL(Create_LISTS_TABLE);

        final String Create_ITEMS_TABLE = "CREATE TABLE " + ListItems.TABLE_NAME + " (" +
                ListItems._ID + " integer primary key, " +
                ListItems.COLUMN_LIST_ID + " integer," +
                ListItems.COLUMN_NAME + " TEXT, " +
                ListItems.COLUMN_TYPE + " TEXT, " +
                ListItems.COLUMN_QUANTITY + " INTEGER DEFAULT 1, " +
                ListItems.COLUMN_STATUS + " INTEGER DEFAULT " + ListItems.STATUS_PENDING + "," +
                ListItems.COLUMN_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                ListItems.COLUMN_UPDATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
                " FOREIGN KEY (" + ListItems.COLUMN_LIST_ID + ") REFERENCES " + List.TABLE_NAME + "(" + List._ID + "));";

        db.execSQL(Create_ITEMS_TABLE);
    }
}

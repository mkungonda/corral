package project.mkungonda.corral;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import project.mkungonda.corral.data.DatabaseDescription.List;

public class StatsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public interface StatsFragmentListener {

    }

    private static final int LIST_LOADER = 0;

    private StatsFragmentListener listener;
    private Uri listUri;
    private int currentStatus;
    private TextView nameTextView;
    private TextView typeTextView;
    private TextView statusTextView;
    private CoordinatorLayout coordinatorLayout;
    private LinearLayout statsContainer;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (StatsFragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        coordinatorLayout   = (CoordinatorLayout) getActivity().findViewById(R.id.coordinatorLayout);
        Bundle arguments    = getArguments();
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_stats, container, false);
        statsContainer  = (LinearLayout) view.findViewById(R.id.statsContainer);

        getLoaderManager().initLoader(LIST_LOADER, null, this);
        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;

        switch (id) {
            case LIST_LOADER:
                cursorLoader = new CursorLoader(getActivity(), List.STATS_URI, null, null, null, null);
                break;
            default:
                cursorLoader = null;
                break;
        }

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        System.out.println(stringFromJNI());
        if(data != null && data.moveToFirst()) {

//            ArrayList<String> mArrayList = new ArrayList<String>();
//
//            do {
//                mArrayList.add(mCursor.getString(mCursor.getColumnIndex(dbAdapter.KEY_NAME))); //add the item
//            } while (data.moveToNext());
//            Log.v("Stats Data", DatabaseUtils.dumpCursorToString(data));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}


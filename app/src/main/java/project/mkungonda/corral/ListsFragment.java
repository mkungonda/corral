package project.mkungonda.corral;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import project.mkungonda.corral.data.DatabaseDescription.List;

public class ListsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

        public interface ListFragmentListener {
            void onListSelected(Uri listUri);
            void onAddList();
            void onStatsShow();
        }

        private static final int LIST_LOADER = 0;
        private ListFragmentListener listener;
        private ListsAdapter listAdapter;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            setHasOptionsMenu(true);

            View view = inflater.inflate(R.layout.fragment_list, container, false);
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));

            listAdapter = new ListsAdapter(new ListsAdapter.ListClickListener() {
                        @Override
                        public void onClick(Uri listUri) {
                            listener.onListSelected(listUri);
                        }
                    }
            );
            recyclerView.setAdapter(listAdapter);

            recyclerView.addItemDecoration(new ItemDivider(getContext()));

            recyclerView.setHasFixedSize(true);

            FloatingActionButton addButton = (FloatingActionButton) view.findViewById(R.id.addButton);
            addButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onAddList();
                        }
                    }
            );

            FloatingActionButton statsButton = (FloatingActionButton) view.findViewById(R.id.statsButton);
            statsButton.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View view) {
                                                 listener.onStatsShow();
                                             }
                                         }
            );

            return view;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            listener = (ListFragmentListener) context;
        }

        @Override
        public void onDetach() {
            super.onDetach();
            listener = null;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getLoaderManager().initLoader(LIST_LOADER, null, this);
        }

    public void updateList() {
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LIST_LOADER:
                return new CursorLoader(getActivity(), List.CONTENT_URI, null, null, null, List.COLUMN_UPDATED_AT + " DESC");
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        listAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        listAdapter.swapCursor(null);
    }

}

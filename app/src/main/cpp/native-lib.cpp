#include <jni.h>
#include <string>

extern "C"
jstring
Java_project_mkungonda_corral_StatsFragment_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
